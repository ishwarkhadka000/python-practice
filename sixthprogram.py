##FUNCTION
# import webbrowser
# def hit_infodev():
#     webbrowser.open("htts://infodev.com.np")
    
    

# if __name__ == '__main__':
#     hit_infodev()

##KEYWORD ARGUMENT
# def student(firstname, lastname="Smith", standard="fifth"):
#     print(f'{firstname} {lastname} studies at {standard}')


##1 keyword argument
# student(firstname="john")

##2 keyword arguments
# student(firstname="Brawn", lastname= "Mark")

##3 keyword arguments
# student(firstname="Ishwar" , lastname= "Khadka" , standard = "Bachelor")

##ARBITRARY ARGUMENTS *args
# def sum(*numbers):
#     result = 0
#     for number in numbers:
#         result += number
#     return result
# print(sum(1,2,4))
# print(sum(1,2,3,4,5,6,7,8,9,10))

# def average_calculator(*numbers):
#     sum = 0
#     for number in numbers: 
#         sum+= number
#     return sum/len(numbers)

# print(average_calculator(1,2,3,4,5,6,7))
  
# def average_calculator(*numbers):
#     sum = 0
#     n = 0
#     for number in numbers: 
#         n+=1
#         sum+= number
#     return sum/n

# print(average_calculator(1,2,3,4,5,6,7))

##Arbitrary Keyword Arguments , **kwargs

# def kwargs_example(**person):
#     print(type(person))
#     for key , value in person.items():
#         print(f'{key} = {value}')
    
#     return person
# value = kwargs_example(name='Ishwar', age = 24)
# print(value)         

##MATH FUNCTIONS

# import math
# print(math.pi)
# print(math.sin(90))

##RECURSION FUNCTIONS

def factorial(number):
    if number == 0:
        return 1
    elif number == 1:
        return 1
    else:
        return(number * factorial(number-1))
    
print(f'The factorial of 5 is {factorial(5)}')
    