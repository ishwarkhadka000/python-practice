#inverted star pattern
# using for loop
for i in range(7,0,-1):
    for k in range(7,i,-1):
        print(end='    ')
    for j in range(i):
         print('*', end='   ')
    print()


# using while loop
i = 7
while i >=1:
    j = 7
    while j>i:
        print(end='    ')
        j-=1
    k = 1
    while k<=i:
        print("*", end = '   ')
        k+=1
    i-=1
    print()