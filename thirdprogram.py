a = "IsHwaR"
print(a)
print(id(a))

a+=" Khadka"
print(a)
print(id(a))
for i in a:
    print(i)

print(a.upper())
print(a.lower())
print(a.capitalize())
print(a.title())
print(a.strip())
print(a.replace('s','$'))

#LISTS
fruits =  ['apple', 'banana', 'orange', 'mango']

print(fruits[1])
print(fruits[-1])
print(fruits[1:3])
print(fruits.pop())
fruits.append('grapes')
print(fruits)
fruits.insert(3, 'rasberry')
print(fruits)
fruits.remove('rasberry')
print(fruits)


#DICTIONARY

data = {
    "first_name": "Ishwar",
    "lastname" : "Khadka",
    "address" : "Bhaktapur"
}

print(type(data))
print(data)
print(data['first_name'])
print(data.keys())
print(data.values())
print(data.items())
print(len(data))
data.clear()
print(data)
del data

data["first_name"] = "Ram"

print(data)

data["contact"] = "980XXXXXXX"
print(data)


#TUPLE

numbers = (1,2,3,4,5,6,7,8,9,10,10)
print(numbers)
numlist = list(numbers)
print(numlist)
numlist.append(11)
numbers = tuple(numlist)
print(numbers)

print(type(numbers))
print(type(numlist))

#SETS

fruits = {'apple','orange','mango','banana'}
print(fruits)

for fruit in fruits:
    if fruit == 'apple':
        print(fruit)
        
fruits.add('rasberry')
print(fruits)


students = [
    {
        "name": "Ram",
        "marks": 50
    },
    {
        "name":"Hari",
        "marks":60
    },
    {
        "name":"Sita",
        "marks": 90
    }
]
print(students[0])
print(students[0]["name"])
students[1]["marks"] = 55
print(students)
students.append({"name":"Gita", "marks":90})
print(students)
