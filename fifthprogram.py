#FOR LOOP

#range(start,stop,step)

for i in range(0, 11,2):
    print(i)


numbers = [1,3,5,6,3,9,10,100]
sum = 0
for number in numbers:
    sum+=number
print(f'The sum is {sum}')


sum = 0
for i in range(0,101):
    sum += i
print(f"The sum of numbers between 0 and 100 is {sum}")

#WHILE LOOP

#NESTED LOOP

for i in range(1,6):
    breakpoint()
    for j in range(i):
        print(i, end =' ')
    print()

#INFINITE LOOP
while True:
    num = int(input("Enter a number(0 to exit):"))
    if num == 0:
        print("Program exiting")
        break
    is_prime = True
    
    for i in range(2,num//2+1):
        if num % i == 0:
            is_prime = False
            break
    if is_prime:
        print(f'{num} is a prime number')
    else:
        print(f'{num} is not a prime niumber')

#BREAK AND CONTINUE

for i in range(11):
    print(i)
    if i > 5:
        print("before break")
        break
    print("after continue")
print("finished")


for i in range(11):
    print(i)
    if i > 5:
        print("before break")
        continue
    print("after continue")
print("finished")

#inverted star pattern
# using for loop
for i in range(7,0,-1):
    for k in range(7,i,-1):
        print(end='  ')
    for j in range(i):
         print('*', end=' ')
    print()


# using while loop
i = 7
while i >=1:
    j = 7
    while j>i:
        print(end='    ')
        j-=1
    k = 1
    while k<=i:
        print("*", end = '   ')
        k+=1
    i-=1
    print()
    