def prime_number(num):
    is_prime = True
    for n in range(2,num//2+1):
        if num % n == 0:
            is_prime = False
            break
    return is_prime

a = int(input("enter a number:"))

if prime_number(a):
    print(f'{a} is a prime number')
else:
    print(f'{a} is not a prime number')