a = int(input("Enter a:"))
b = int(input("Enter b:"))

print(f'Before swapping: \n a = {a} \n b = {b}')

a = a+b
b = a-b
a = a-b

print(f'After swapping: \n a = {a} \n b = {b}')
