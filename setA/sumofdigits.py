def sumOfDigits(num):
    sum = 0
    flag = True
    while flag:
        rem = num%10
        quotient = int(num/10)
        sum+=rem
        if quotient == 0 :
            flag = False
        else:
            num = quotient
    return sum

number = int(input("Enter a number:"))
print(f'The sum of digits of {number} is: {sumOfDigits(number)}')           