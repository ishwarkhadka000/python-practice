
def sortArray(numList):
    for i in range(0,len(numList)):
        for j in range(0,len(numList)):
            if numList[j]> numList[i]:
                a = numList[i]
                numList[i] = numList[j]
                numList[j] = a
                
    return numList


numbers = []
n = int(input("Enter no. of elements in a list:"))

for i in  range(0,n):
    num = int(input(f"Enter element{i+1}"))
    numbers.append(num)
    
print(sortArray(numbers))