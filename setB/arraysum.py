numList = [-3,-5,-7, 0,3,5,7]

sum = 0
for num in numList:
    if num > 0:
        sum+=num
        
print(f'The sum of numbers greater than zero in the list is : {sum}')