def prime_number(num):
    is_prime = True
    for n in range(2,num//2+1):
        if num % n == 0:
            is_prime = False
            break
    return is_prime

numList = [4,6,8,9,11,13,15]
for number in numList:
    if(prime_number(number)):
        firstPrime = number
        break

print(f'The first prime number on the list is {firstPrime}')
        
