#IF STATEMENT
a = int(input("Enter a number:"))
b = int(input("Enter a number:"))
c = int(input("Enter a number:"))

print("The largest number is :")
if(a>b and a>c):
    print(a)
    
if (b>a and b>c):
    print(b)

if (c>a and c>b):
    print(c)


#ELSE-IF STATEMENT

username = input("Enter username:")
password = input("Enter password:")

if (username == "admin" and password == "admin"):
    print("Logged in successfully !!")
else:
    print("Invalid credentials!!")

#ELIF STATEMENT

#NESTED 

students = [
    {"name": "Dipak", "age": 17, "gender": "male"},
    {"name": "nisma", "age": 15, "gender": "female"},
    {"name": "nishan", "age": 14, "gender": "male"},
    {"name": "swete", "age": 13, "gender": "female"},
]

for student in students:
    if student.get("gender") == "male":
        if student.get("age") >= 15:
            print(student.get("name"), "can vote here")
        else:
            print(student.get("name"), "can not vote here")
    else:
        if student.get("age") >= 14:
            print(student.get("name"), "can vote here")
        else:
            print(student.get("name"), "can not vote here")


#MATCH CASE STATEMENTS
day = int(input("Enter an integer:"))

match day:
    case 1:
        print(f'the first day of the week is Sunday')
    case 2:
        print(f'the second day of the week is Monday')
    case 3:
        print(f'the third day of the week is Tuesday')
    case 4:
        print(f'the fourth day of the week is Wednesday')
    case 5:
        print(f'the fifth day of the week is Thursday')
    case 6:
        print(f'the sixth day of the week is Friday')
    case 7:
        print(f'the seventh day of the week is Saturday')
    case _:
        print("Invalid input value")
